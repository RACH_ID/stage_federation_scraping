import pandas as pd

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# Lire le fichier Excel
table_fichiers = pd.read_excel('Liens_mass_start.xlsx')

# Extraire la première colonne en une liste
liste_Urls = table_fichiers.iloc[:, 1].tolist() 

    
# Initialiser le pilote de navigateur (dans cet exemple, Firefox)
driver = webdriver.Firefox()

Liste_code_sources = []
for url in liste_Urls :

    try:
        # Ouvrir la page dans le navigateur
        driver.get(url)
        
        # Trouver tous les éléments "toggle"
        toggle_elements = WebDriverWait(driver, 10).until(EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "toggle")]')))
        
        # Cliquer sur chaque élément "toggle"
        for toggle_element in toggle_elements:
          toggle_element.click()
        
        # Attendre que les contenus deviennent visibles après le clic
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[contains(concat( " ", @class, " " ), concat( " ", "name", " " ))]')))
        
        # Récupérer le code source de la page
        page_source = driver.page_source
        Liste_code_sources.append(page_source)
        
    except Exception as e:
      print(f"La récupération du contenu de {url} a échoué. Erreur : {e}")

# Fermer le navigateur
driver.quit()
