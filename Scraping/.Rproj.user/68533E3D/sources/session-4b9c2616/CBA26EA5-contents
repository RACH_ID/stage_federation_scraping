library(shiny)   
library(dplyr)   
library(tidyr)   
library(ggplot2) 

# # Charger les données à partir du fichier "DATA MASS START.csv"
# DATA_MASS_Start <- read.csv("DATA MASS START.csv")
# 
# # Trouver les indices où Rnk (colonne de classement) est égal à 1, l'indice la ou il commence chaque table
# indices <- which(DATA_MASS_Start$Rnk == 1)
# 
# # Initialiser une liste pour stocker les écarts de temps pour chaque tour
# ecart_temps_tours_total <- list()
# 
# 
# for (i in 1:length(indices)) {
#   
#   if(i < length(indices)) {
#     Data <- DATA_MASS_Start[indices[i]:(indices[i+1]-1),]
#   } else {
#     Data <- DATA_MASS_Start[indices[i]:nrow(DATA_MASS_Start),]
#   }
#   
#   
#   ecart_temps_tour <- list()
#   
#   # Parcourir les colonnes correspondant à chaque tour
#   for (k in c(6, seq(10, 52, by = 3))) {
#     ecart_tour <- c()
#     # Calculer l'écart de temps pour chaque tour
#     if(k == 6) {
#       Data <- Data %>% arrange(AT_tour.1)
#       for (j in 2:(nrow(Data))) {
#         if(!is.na(Data[j, 6]) & (Data[j, 6] - Data[1, 6] > 0)) {
#           ecart_tour <- c(ecart_tour, Data[j, 6] - Data[1, 6])
#         }
#       }
#       if(length(ecart_tour > 0)) { 
#         ecart_temps_tour[[paste0("tour", 1)]] <- ecart_tour
#         ecart_temps_tours_total[[i]] <- ecart_temps_tour
#       }
#     } else {
#       Data <- Data %>% arrange(!!sym(paste0("Time_tour.", (k-4) %/% 3)))
#       for (j in 2:(nrow(Data))) {
#         if(!is.na(Data[j, k]) & (Data[j, k] - Data[1, k] > 0)) {
#           ecart_tour <- c(ecart_tour, Data[j, k] - Data[1, k])
#         }
#       }
#       if(length(ecart_tour > 0)) { 
#         ecart_temps_tour[[paste0("tour", (k-4) %/% 3)]] <- ecart_tour
#         ecart_temps_tours_total[[i]] <- ecart_temps_tour
#       } 
#     }
#   }
# }
# 
# # Obtenir les noms uniques des compétitions
# names_competition <- unique(DATA_MASS_Start$Compétition) 
# 
# # Initialiser une liste pour stocker les tables par compétition
# names_tables <- list()
# for (n in 1:length(names_competition)) {
#   names_tables[[n]] <- unique(DATA_MASS_Start$Titre.de.table
#                               [DATA_MASS_Start$Compétition == names_competition[n]])
# }
# 
# # Calculer le nombre de tours pour chaque course
# Tours <- unlist(lapply(ecart_temps_tours_total, length))


ui <- fluidPage(
  titlePanel("Densité et Fonction de Répartition des ecart du temps par tour : -> Mass Start <-"),
  sidebarLayout(
    sidebarPanel(
      selectInput("competition", "Choisir la compétition :", choices = names_competition, multiple = TRUE),
      uiOutput("select_table"),
      uiOutput("select_tours"),
      actionButton("afficher", "Afficher")
    ),
    mainPanel(
      
      # Graphique de densité
      plotOutput("graphique_densite"),
      # Graphique de fonction de répartition
      plotOutput("graphique_repartition"),
      # Zone pour afficher les vecteurs d'ecart du temps
      verbatimTextOutput("ecart_selected")
    )
  )
)

server <- function(input, output, session) {
  # Fonction réactive pour sélectionner la table basée sur la compétition
  selected_ind_com <- reactiveVal()
  output$select_table <- renderUI({
    if (is.null(input$competition)) return(NULL)
    tables_choices<- c()
    indices_comp<-which(names_competetion %in% input$competition)
    
    for (i in indices_comp) {
      tables_choices <- c(tables_choices,names_tables[[i]])  
    }
    selectizeInput("table", "Choisir une table :", choices = tables_choices, multiple = TRUE)
  })
  
  # Sélection du nombre de tours pour la table sélectionnée
  output$select_tours <- renderUI({
    table_selected <- input$table
    
    #selected_ind_com(indices_comp)
    if (is.null(table_selected)) return(NULL)
    selectInput("tours", "choisir les tours désirées :",  choices = 1:16, multiple = TRUE)
  })
  
  observeEvent(input$afficher, {
    if (is.null(input$competition) || is.null(input$table)) {
      return(NULL)
    }
    
    # Obtenir les tours sélectionnés
    table_selected <- input$table
    
    if (length(table_selected) == 0) {
      output$graphique <- renderPlot(NULL)
      return()
    }
    # Sélectionner les écarts de temps pour les tours sélectionnés
    
    indices_comp<- which(names_competetion %in% input$competition)
    tours_max <-c()
    for (ind in indices_comp) {
      if (ind == 1) {
        tours_max <-c(tours_max, which(names_tables[[ind]] %in% input$table))
      } else {
        tours_max <-c(tours_max,sum(lengths(names_tables[1:(ind - 1)])) + which(names_tables[[ind]] %in% input$table))
      }
    }
    ecart_selected <- list(length(tours_max)+length(input$tours))
    index_ecart <- 1
    for (t in input$tours) 
    {
      for (i in 1:length(tours_max)) {
          Z<-tours_max[i]
          ecart_selected[[index_ecart]] <- round(ecart_temps_tours_total[[Z]][[paste("tour", t, sep = "")]],4)
          index_ecart <- index_ecart + 1
      } 
    }
    
    # Créer un graphique de densité pour chaque tour sélectionné
    p_d <- ggplot()
    for (t in ecart_selected) {
      p_d <- p_d + geom_density(data = as.data.frame(t), aes_string(x = t), col = sample(colors(), 1), alpha = 0.5)
    }
    
    # Créer un graphique de fonction de répartition pour chaque tour sélectionné
    p_r <- ggplot()
    for (t in ecart_selected) {
      p_r <- p_r + stat_ecdf(data = as.data.frame(t), aes_string(x = t), col = sample(colors(), 1), alpha = 0.5)
    }
    
    # Afficher les vecteurs d'écart de temps sélectionnés
    
    R <- paste("Les vecteurs d'ecart de temps pour les tours :",paste(input$tours,collapse = ", "),
    "pour les tables : \n",paste(input$table,collapse = ", "), "dans les compétitions",paste(input$competition,collapse =", "),"sont :\n",
    paste("->",ecart_selected,collapse = "\n "))
    

    # Afficher les options sélectionnées
    output$ecart_selected <- renderText({
      print(ecart_selected)
      return(print(R))
    })
    
    # Afficher le graphique de densité
    output$graphique_densite <- renderPlot({
      p_d + labs(title = paste("Densité d'ecart de temps des Tours :",paste(input$tours,collapse =", "), " pour :\n", paste(input$table,collapse =", "), "dans", paste(input$competition,collapse =", ")))
    })
    
    # Afficher le graphique de fonction de répartition
    output$graphique_repartition <- renderPlot({
      p_r + labs(title = paste("Fonction de Répartition d'ecart de temps des Tours :",paste(input$tours,collapse =", "), " pour :\n", paste(input$table,collapse =", "), "dans", paste(input$competition,collapse =", ")))
    })
  })
}

shinyApp(ui = ui, server = server)
