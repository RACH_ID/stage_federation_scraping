#------------------------------------------- LES LIBRAIRIES -----------------------------------------------------------------------------------

library(readxl) #Importation de la bibliothèque readr pour lire le fichier EXCEL
library(readr)  # Importation de la bibliothèque readr pour lire les fichiers
library(stringr)  # Importation de la bibliothèque stringr pour manipuler les chaînes de caractères
library('reticulate') # Importation de la bibliothèque reticulate pour excuter les script python

#------------------------------------------- Lecture des fichiers -----------------------------------------------------------------------------------

py_run_file('Script python.py')
liste_fichiers <- py$Liste_code_sources

#------------------------------------------- Définition et implémantation de la fonction scraping -----------------------------------------------------------------------------------
scraping <- function(fichier)
{
  texte <- read_file(fichier)  # Chargement du contenu du code source dans la variable texte
  v <- str_split(texte, 'Points:')[[1]]  # Séparation du texte en fonction de 'Points:' et sélection du contenu d'intérêt
  interet <- v[[length(v)]]  # Extraction du contenu d'intérêt
  w <- str_split(interet, 'app-mass-start-result-table')[[1]]  # Séparation supplémentaire du contenu
  interet2 <- w[[1]]  # Sélection du contenu final
  
  infos_brut <- v[[1]]
  titre <- str_extract_all(infos_brut, "(?<=\\<h3 _ngcontent-vog-c47=).*?(?=\\</h3>)")
  Date_localisation <- str_extract_all(infos_brut, "(?<=\\</i>).*?(?=\\</span>)")
  
  
  #-------------------------------Extraction du contenu entre les balises <td>----------------------------
  
  contenus <- str_extract_all(interet2, "(?<=\\>).*?(?=\\<)")  # Extraction du contenu entre les balises <td>
  contenus <- unlist(contenus)  # Transformation de la liste en vecteur
  contenus <- contenus[which(contenus!="")]  # Suppression des éléments vides
  
  #-------------------------------traiteement du contenu-----------------------------------------------
  
  k <- 0  # Initialisation du compteur pour les élément de LISTE
  LISTE <- list()  # Initialisation de la liste pour stocker les données des athlètes
  for(i in 1:length(contenus))
  {
    # Vérification si la ligne contient '/'
    if(str_detect(contenus[i],'/'))
    {
      debut <- i-1  # Début de la sélection des données de l'athlète
      for(j in (i+1):length(contenus))
      {
        if(str_detect(contenus[j],'/'))
        {
          fin <- j-2  # Fin de la sélection des données de l'athlète
          k <- k+1  # Incrémentation du compteur pour les élément de LISTE
          vec <- contenus[debut:fin]  # Extraction des données de l'athlète
          LISTE[[k]] <- vec  # Stockage des données de l'athlète k
          break
        } 
        else if(j == length(contenus))
        {
          fin <- j
          k <- k+1
          vec <- contenus[debut:fin]
          # Nettoyage des données pour le dernier athlète
          for(p in 1:length(vec))
          {
            vec[p] <- trimws(vec[p],'both')  # Suppression des espaces inutiles
          }
          LISTE[[k]] <- vec  # Stockage des données de l'athlète dans la liste
          break
        }
      }
      # Parcours de chaque élément de L
      for (i in seq_along(LISTE)) {
        # Vérifie si l'élément est une liste de NA
        if (is.list(LISTE[[i]]) && all(is.na(unlist(LISTE[[i]])))) {
          # Supprime l'élément de L
          L <- L[-i]
        }
      }
    }
  }
  T_<-character()
  R_<-character()
  Data_player<-list()
  for(i in 1:(length(LISTE)))
  {
    ess <- LISTE[[i]]  # Sélection des données de l'athlète
    ess <- ess[-c(1:5)]  # Suppression des premières colonnes qui ont déjà été traitées
    ess <- ess[which(str_detect(ess, " "))]  # Sélection des éléments contenant des espaces
    ess_clean <- ess[!grepl("[a-zA-Z]", ess)]  # Suppression des éléments contenant des lettres
    ess_clean <- unique(ess_clean)  # Suppression des doublons
    if (length(ess_clean>0))
    {Data_player[[i]]<- ess_clean}
    else
    {
      LISTE[[i]][1]<-i
      ess_clean<-rep('0',length(Data_player[i-1]))
      Data_player[[i]]<- ess_clean
    }
    
  }
  #--------------------------------------Remplissage des contenues dans la table de données-------------------------------
  l<-max(sapply(Data_player, length))
  g<-2*l+5
  DATA_MASS_ <- data.frame(matrix(nrow = length(LISTE), ncol = g))  # Création d'un dataframe vide
  names(DATA_MASS_)[1:5] <- c('Rnk', 'Cap_et_Nb', 'Name', 'COUNTRY', 'Total Time', 'Points')  # Attribution des noms de colonnes
  DATA_MASS <- data.frame(matrix(nrow = length(LISTE), ncol = g+l))  # Création d'un dataframe vide
  vv <- c()  # Initialisation du vecteur pour stocker les noms des colonnes du vecteur du temps intermédiaire cumulé (le temps de chaque partie
  for(n in 1:l)
  {
    vv[2*n-1] <- paste('AT_tour', n)    
    vv[2*n] <- paste('Rank_tour', n)
    
  }
  names(DATA_MASS_)[6:g] <- vv  # Attribution des noms de colonnes du vecteur du TIC
  for(i in 1:(length(LISTE)))
  {
    for (k in 1:l) 
    {
      T_[k] <-(strsplit(Data_player[[i]][k], " \\(")[[1]])[1]  # Extraire les temps en enlevant les rangs
      R_[k]<-gsub("\\)", "", strsplit(Data_player[[i]][k], " \\(")[[1]][2])  # Extraire les rangs et convertir en numérique  
    }
    DATA_MASS[i, 1:5] <- LISTE[[i]][1:5]
    DATA_MASS_[i, 1:5] <- LISTE[[i]][1:5]  
    for (j in seq(from=6,to=g,by=2) ) 
    {
      index <- (j-4)/2
      DATA_MASS_[i, j ] <-T_[index]
      DATA_MASS_[i, j+1 ] <-R_[index]
    }
  }
  convert_to_numeric <- function(df) 
  {
    ifelse(grepl(":", df), as.numeric(unlist(strsplit(df, ":")))[1] * 60 + as.numeric(unlist(strsplit(df, ":")))[2], as.numeric(df))
  }
  for (j in 5:g) {
    DATA_MASS_[,j]<-sapply(DATA_MASS_[, j], convert_to_numeric)
  }
  DATA_MASS_[is.na(DATA_MASS_)] <- 0
  Q<-c()
  AT_ <- DATA_MASS_[,8]-DATA_MASS_[,6]
  DATA_MASS <- cbind(DATA_MASS_[, 1:9], AT_, DATA_MASS_[, 10:ncol(DATA_MASS_)])
  colnames(DATA_MASS)[10] <- paste('Time_tour',2)  # Renomme la nouvelle colonne
  AT_ <- DATA_MASS_[,8]
  h= g+l -1
  for (j in seq(from = 13, to = h, by = 3))
  {
    if(j== h)
    {
      AT_ <- DATA_MASS[,j-2]-AT_
      AT <- paste('Time_tour',(j-2)%/%3)
      DATA_MASS <- cbind(DATA_MASS, AT_)
      colnames(DATA_MASS)[j] <- AT  # Renomme la nouvelle colonne
    }
    else
    {
      AT_ <- DATA_MASS[,j-2]-AT_
      AT <- paste('Time_tour',(j-2)%/%3)
      DATA_MASS <- cbind(DATA_MASS[, 1:j-1], AT_, DATA_MASS[, (j):ncol(DATA_MASS)])
      colnames(DATA_MASS)[j] <- AT  # Renomme la nouvelle colonne
      AT_ <- DATA_MASS[,j-2]
    }
    
  }
  #-----------------------Table de données final + les attribus des infos de compétition----------------------------------------------------
  DATA_MASS[, 5:ncol(DATA_MASS)] <- replace(DATA_MASS[, 5:ncol(DATA_MASS)], DATA_MASS[, 5:ncol(DATA_MASS)] <= 0, NA)
  
  infos_brut <- v[[1]]
  titre <- str_extract_all(infos_brut, "(?<=<h3).*?(?=</h3>)")
  Date_localisation <- str_extract_all(infos_brut, "(?<=</i>).*?(?=</span>)")
  
  competition_info <- str_split(infos_brut, 'row competition-info')  #competition-info__self
  infos <- str_extract_all(competition_info[[1]][2], "(?<=>).*?(?=<)")[[1]]
  infos=unlist(infos)
  infos=infos[which(infos!="")][1:8]
  
  Date <- Date_localisation[[1]][1]
  Localisation <- Date_localisation[[1]][2]
  titre_table <- gsub("_ngcontent-deu-c47=\"\">","", titre[[1]][1])
  titre_comp <- infos[1]
  Referee <-c(infos[3],infos[4])
  Starter <-c(infos[6],infos[7])
  time_comp <-infos[8]
  
  attr(DATA_MASS, "title") <- titre_table
  attr(DATA_MASS, "subtitle") <- list("competition" = titre_comp,
                                      "Localisation" = Localisation,
                                      "Date de compétition" = Date,
                                      "time competition" = time_comp,
                                      "Referee" = Referee[1],
                                      "Referee_NAT" = Referee[2],
                                      "Starter" = Starter[1],
                                      "Starter_NAT" = Starter[2])
  competition <- str_split(attributes(DATA_MASS)$title, '">')[[1]][2]
  Game <-attributes(DATA_MASS)$subtitle$competition
  JNS <- regmatches(competition, regexpr("(Junior|Neo Senior)", competition))
  JNS_ <- regmatches(Game,regexpr("(Junior|Neo-seniors)", Game))
  
  categorie <- ifelse(length(JNS_) > 0, JNS_[1], ifelse(length(JNS) > 0, JNS[1], "Senior"))
  
  Tour<-ifelse(grepl("SF | Semi-Final", Game),'SF', 'FINAL')
  Sex <-ifelse(grepl("Women", Game),'Women', 'Men')
  Lieu<-attributes(DATA_MASS)$subtitle$Localisation
  Date<-attributes(DATA_MASS)$subtitle$`Date de compétition`
  J_H<-attributes(DATA_MASS)$subtitle$`time competition`
  Referee<-attributes(DATA_MASS)$subtitle$Referee
  Referee_NAT<-attributes(DATA_MASS)$subtitle$Referee_NAT
  Starter<-attributes(DATA_MASS)$subtitle$Starter
  Starter_NAT<-attributes(DATA_MASS)$subtitle$Starter_NAT
  Division<-ifelse(grepl("Division B", Game),'Division B', 'Division A')
  infos_data <- tibble (
    Compétition = competition,`Titre de table` = Game,Catégorie = categorie,Division = Division,Tour = Tour,Sex = Sex,Lieu = Lieu,Date = Date,`Jour et Heure competition` = J_H,
    Referee = Referee,Ref_Nationalité = Referee_NAT,Starter = Starter,Sta_Nationalité = Starter_NAT)
  
  DATA_MASS <- cbind(DATA_MASS, infos_data)
  
  return(DATA_MASS)
}
#------------------------------------------- Scraper les données d'une liste des codes sources -----------------------------------------------------------------------------------
Liste_Data <- lapply(liste_fichiers, scraping)
#------------------------------------------- Stocker les données dans un seul fichier CSV ---------------------------------------------------------------------------------------------------------------------
All_Data<-data.frame()
for (i in 1: length(Liste_Data)) 
{
  All_Data <-bind_rows(All_Data,Liste_Data[[i]])
}
if (file.exists("DATA MASS START.csv")) {
  file.remove("DATA MASS START.csv")
}
write.csv(All_Data, "DATA MASS START.csv", row.names = FALSE)
#------------------------------------------- FIN DU PROGRAMME ---------------------------------------------------------------------------------------------------------------------
