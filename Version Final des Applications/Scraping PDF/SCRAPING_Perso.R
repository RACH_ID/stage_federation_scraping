library(shiny)
library(pdftools)
library(stringr)
library(dplyr)
library(writexl)
# UI
ui <- fluidPage(
  titlePanel("Extraction des données des PDF"),
  sidebarLayout(
    sidebarPanel(
      fileInput("file", "Choisir un fichier PDF"),
      selectInput("columns", "séléctionner les Colonnes", choices = NULL, multiple = TRUE),
      downloadButton("download", "Télécharger le tableau")
    ),
    mainPanel(
      tableOutput("text_table")
    )
  )
)

# Server
server <- function(input, output, session) {
  
  extracted_text <- eventReactive(input$file, {
    req(input$file$datapath)
    
    text <- pdf_text(input$file$datapath)
    
    # Traiter chaque ligne de chaque page
    modified_lines <- unlist(sapply(text, function(page) {
      lines <- str_split(page, "\n")[[1]]
      modified_lines <- sapply(lines, function(line) {
        line <- str_trim(line, side = "left")
        line <- str_replace_all(line, " ", "*")  # Remplacer les espaces simples par des étoiles (*)
        line <- str_replace_all(line, "\\*{2,}", "+")  # Remplacer deux étoiles ou plus par des signes plus (+)
        return(line)
      })
      return(modified_lines)
    }))
    
    return(modified_lines)
  })
  
  processed_data <- reactive({
    req(extracted_text())
    
    lines <- extracted_text()
    # Diviser chaque ligne sur les signes plus (+) et remplacer les étoiles (*) par des espaces
    list_data <- lapply(lines, function(line) {
      elements <- str_split(line, "\\+")[[1]]
      elements <- str_replace_all(elements, "\\*", " ")
      # Nettoyer les éléments vides qui ne contiennent que des espaces
      #elements <- elements[!str_detect(elements, "^\\s*$")]
      elements <- replace(elements, elements == "", NA)
      return(elements)
    })
    
    # Identifier la liste de noms de colonnes
    column_names <- NULL
    for (i in seq_along(list_data)) {
      if ('Name' %in% list_data[[i]] || 'SKATER' %in% list_data[[i]] || 'Firstname' %in% list_data[[i]]) {
        column_names <- list_data[[i]]
        if (length(column_names) >= 10) {
          column_names <- column_names[seq_len(8)]
        }
        i_with_names <- i
        break
      }
    }
    
    df_data <- list()
    
    # Si aucune ligne valide pour les noms de colonnes n'est trouvée
    
    if (is.null(column_names)) {
      for(h in 1:8)
      {
         column_names<-c(column_names,paste("Colonne.",h))
      }
      for (i in seq_along(list_data)) {
          if (length(list_data[[i]]) >= 4) {
            i_with_names <- i-1
            break
          }
      }
      for (j in (i_with_names+1):length(list_data)) {
        line <- list_data[[j]]
        if (length(line) >= 3) {
          df_data <- append(df_data, list(line))
        }
      }
      #stop("Aucune colonne nommée 'Name' n'a été trouvée dans les noms de colonnes.")
    }else
    {
      for (j in (i_with_names+1):length(list_data)) {
        line <- list_data[[j]]
        if (length(line) >= (length(column_names)-2)) {
          df_data <- append(df_data, list(line))
        }
      }
    }
    
    df <- as.data.frame(do.call(rbind, df_data), stringsAsFactors = FALSE, na.strings = "")
    colnames(df) <- column_names[seq_len(ncol(df))]
    # Mettre à jour les choix de colonnes dans le selectInput
    updateSelectInput(session, "columns", choices = colnames(df),selected = colnames(df))
    
    return(df)
  })
  
  output$text_table <- renderTable({
    req(processed_data())
    selected_columns <- input$columns
    if (length(selected_columns) == 0) {
      return(NULL)
    } else {
      return(processed_data()[, selected_columns, drop = FALSE])
    }
  })
  
  output$download <- downloadHandler(
    filename = function() {
      paste("PDF_modifie_", Sys.Date(), ".xlsx", sep = "")
    },
    content = function(file) {
      selected_columns <- input$columns
      if (is.null(selected_columns)) {
        showNotification("Veuillez sélectionner au moins une colonne.", type = "error")
        return()
      }
      write_xlsx(processed_data()[, selected_columns, drop = FALSE], file)
    })
}

shinyApp(ui, server)
