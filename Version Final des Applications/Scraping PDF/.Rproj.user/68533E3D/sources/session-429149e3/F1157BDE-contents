library(readxl)
library(dplyr)
library(ggplot2)
library(tidyr)
library(gridExtra)


# Charger la base de données
df <- read_excel("Base de données resultats globale FFRS.xlsx")
df<-as.data.frame(df)

datafr <- df %>% filter(Nationalité %in% c("France","FRA"))


athletes_uniquefr <- datafr %>%
  select(Athlète, Sexe, `catégorie d’âge`,Discipline) %>%
  distinct()

table_sexe <- athletes_uniquefr %>%
  group_by(Sexe) %>%
  summarise(Count = n())

ggplot(table_sexe, aes(x = Sexe, y = Count, fill = Sexe)) +
  geom_bar(stat = "identity") +
  labs(title = "Distribution des Athlètes par Sexe", x = "Sexe", y = "Nombre d'Athlètes")

table_sexe_discipline <- athletes_uniquefr %>%
  group_by(Sexe, Discipline) %>%
  summarise(Count = n()) %>%
  mutate(Percentage = Count / sum(Count) * 100)


# Fonction pour créer un camembert pour une catégorie d'âge spécifique
create_pie_chart <- function(disci) {
  data_filtered <- table_sexe_discipline %>% filter(Discipline == disci)
  
  ggplot(data_filtered, aes(x = "", y = Percentage, fill = Sexe)) +
    geom_bar(width = 1, stat = "identity") +
    coord_polar("y", start = 0) +
    theme_void() +
    labs(title = paste("Répartition des Athlètes dans : ", disci)) +
    geom_text(aes(label = Count ), 
              position = position_stack(vjust = 0.5))
}

# Créer une liste de camemberts pour chaque catégorie d'âge
categories <- unique(table_sexe_discipline$Discipline)
pie_charts <- lapply(categories, create_pie_chart)
do.call(grid.arrange, c(pie_charts, ncol = 2))

table_age_discipline <- datafr%>%
  mutate(Sexe = case_when(
  Sexe == "Femme" ~ "W",
  Sexe == "Homme" ~ "M",
  TRUE ~ Sexe
  ))%>%
  group_by(`catégorie d’âge`, Discipline,Epreuve,Sexe) %>%
  summarise(Count = n()) %>%
  mutate(Percentage = Count / sum(Count) * 100)


# Fonction pour créer un camembert pour une catégorie d'âge spécifique
create_pie_chart <- function(category) {
  data_filtered <- table_age_discipline %>% filter(`catégorie d’âge` == category)
  
  ggplot(data_filtered, aes(x = "", y = Percentage, fill = Discipline)) +
    geom_bar(width = 1, stat = "identity") +
    coord_polar("y", start = 0) +
    theme_void() +
    labs(title = paste("Participation par Catégorie d'Âge:", category)) +
    geom_text(aes(label = paste0(Epreuve," : ",Count,"(",Sexe,")","\t\t")), 
              position = position_stack(vjust = 0.5))
}

# Créer une liste de camemberts pour chaque catégorie d'âge
categories <- unique(table_age_discipline$`catégorie d’âge`)
pie_charts <- lapply(categories, create_pie_chart)
do.call(grid.arrange, c(pie_charts, ncol = 2))

table_comp_discipline <- datafr%>%
  group_by(`Type de compétition`, Discipline,Epreuve,Sexe) %>%
  summarise(Count = n()) %>%
  mutate(Percentage = Count / sum(Count) * 100)


# Fonction pour créer un camembert pour une catégorie d'âge spécifique
create_pie_chart <- function(category) {
  data_filtered <- table_comp_discipline %>% filter(`Type de compétition` == category)
  
  ggplot(data_filtered, aes(x = "", y = Percentage, fill = Discipline)) +
    geom_bar(width = 1, stat = "identity") +
    coord_polar("y", start = 0) +
    theme_void() +
    labs(title = paste("Participation par Compétition:", category)) +
    geom_text(aes(label = paste0(Epreuve," : ",Count,"(",Sexe,")","\t\t")), 
              position = position_stack(vjust = 0.5))
}

# Créer une liste de camemberts pour chaque catégorie d'âge
competition <- unique(table_comp_discipline$`Type de compétition`)
pie_charts <- lapply(competition, create_pie_chart)
do.call(grid.arrange, c(pie_charts, ncol = 2))


table_discipline_epreuve_med <- datafr %>%
  mutate(`Type de compétition` = case_when(
    `Type de compétition` == "European Championships" ~ "EC",
    `Type de compétition` == "Pro Tour" ~ "PT",
    `Type de compétition` == "World Championships" ~ "WC" ,
    TRUE ~ `Type de compétition`
  ))%>%
  group_by(Classement, Discipline, Epreuve,`Type de compétition`) %>%
  summarise(Count = sum(n())) %>%
  mutate(Percentage = Count / sum(Count) * 100)%>%
  filter(Classement %in% c(1, 2, 3))

# Fonction pour créer un camembert pour une nationalité spécifique
create_pie_chart <- function(disc) {
  data_filtered <- table_discipline_epreuve_med %>%
    filter(Discipline == disc) %>%
    group_by(Classement, Epreuve,`Type de compétition`) %>%
    summarise(Count = sum(Count)) %>%
    mutate(Percentage = Count / sum(Count) * 100)
  
  ggplot(data_filtered, aes(x = "", y = Count, fill = Classement)) +
    geom_bar(width = 1, stat = "identity") +
    coord_polar("y", start = 0) +
    theme_void() +
    labs(title = paste("médailles : ", disc)) +
    geom_text(aes(label = paste(Epreuve,": ",Count," (",`Type de compétition`,")")), position = position_stack(vjust = 0.5))
}

disc <- unique(table_discipline_epreuve_med$Discipline)
pie_charts <- lapply(disc, create_pie_chart)
do.call(grid.arrange, c(pie_charts, ncol = 3))


