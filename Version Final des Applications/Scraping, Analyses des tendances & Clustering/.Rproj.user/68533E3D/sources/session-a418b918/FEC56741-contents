library(shiny)
library(dplyr)
library(tidyr)
library(ggplot2)

noms_tables <- unlist(names_tables)

ui <- fluidPage(
  titlePanel("Études des rangs/temps de chaque Patineur : ~ Mass Start ~"),
  sidebarLayout(
    sidebarPanel(
      selectInput("table", "Choisir une ou plusieurs table(s) :", choices = noms_tables, multiple = TRUE),
      uiOutput("select_date"),
      uiOutput("select_nat"),
      uiOutput("player"),
      uiOutput("Rang_time"),
      actionButton("afficher", "Afficher")
    ),
    mainPanel(
      conditionalPanel(
        condition = "input.Rang_time.length > 1",
        plotOutput("evolution_rang"),
        plotOutput("evolution_temps")
      ),
      conditionalPanel(
        condition = "input.Rang_time.length == 1",
        plotOutput("evolution_single", width = "100%")
      ),
      verbatimTextOutput("rang_time_tours")
    )
  )
)

server <- function(input, output, session) {
  output$select_date <- renderUI({
    if (is.null(input$table)) return(NULL)
    date_choices <- unique(subset(DATA_MASS_Start, Titre.de.table %in% input$table)$Jour.et.Heure.competition)
    selectInput("date", "du quel date ", choices = date_choices, multiple = TRUE)
  })
  
  output$select_nat <- renderUI({
    if (is.null(input$date)) return(NULL)
    Nat_choices <- unique(subset(DATA_MASS_Start, Titre.de.table %in% input$table &
                                   Jour.et.Heure.competition %in% input$date)$COUNTRY)
    selectInput("Nat", "Choisir une ou plusieurs nationalités :", choices = Nat_choices, multiple = TRUE)
  })
  
  output$player <- renderUI({
    if (is.null(input$Nat)) return(NULL)
    player_choices <- unique(subset(DATA_MASS_Start, Titre.de.table %in% input$table &
                                      Jour.et.Heure.competition %in% input$date &
                                      COUNTRY %in% input$Nat)$Name)
    selectizeInput("player", "Choisir un ou plusieurs Patineur(se/s)s :", choices = player_choices, multiple = TRUE)
  })
  
  output$Rang_time <- renderUI({
    if (is.null(input$Nat) || is.null(input$player)) return(NULL)
    selectizeInput("Rang_time", "Choisir le Rang/temps :", choices = c("RANGS", "TIMES"), multiple = TRUE)
  })
  
  observeEvent(input$afficher, {
    req(input$table,input$date, input$Nat, input$player, input$Rang_time)
    selected_data <- subset(DATA_MASS_Start, Titre.de.table %in% input$table & COUNTRY %in% input$Nat & Name %in% input$player & Jour.et.Heure.competition %in% input$date)
    
    selected_Rang <- select(selected_data, 3, 61, 7, 9, seq(12, 51, by = 3))
    SR <- gather(selected_Rang, key = "Tour", value = "Rang", -Name, -Jour.et.Heure.competition) %>%
      mutate(Tour = as.numeric(gsub("Rank_tour\\.", "", Tour)),
             Player = paste(Name, Jour.et.Heure.competition, sep = "_"))
    
    q <- ggplot(SR, aes(x = Tour, y = Rang, color = Player, group = Player)) +
      geom_line() +
      labs(title = paste("Évolution du rang de(s) Patineur(se) par tour : \n",
                         paste(input$player, collapse =", "),
                         "\nPour les dates :\n",
                         paste(input$date, collapse =", ")),
           color = "Patineur(se)s") +
      scale_x_continuous(breaks = 1:16, labels = paste0("tour", 1:16)) +
      scale_y_continuous(breaks = 1:30) + 
      theme_minimal() +
      theme(legend.title = element_text("Patineur(se)s"), legend.position = "right")
    
    selected_Time <- select(selected_data, 3, 61, 6, seq(10, 52, by = 3))
    ST <- gather(selected_Time, key = "Tour", value = "Time", -Name, -Jour.et.Heure.competition) %>%
      mutate(Tour = as.numeric(gsub("Time_tour\\.|AT_tour\\.", "", Tour)),
             Player = paste(Name, Jour.et.Heure.competition, sep = "_"))
    
    p <- ggplot(ST, aes(x = Tour, y = Time, color = Player, group = Player)) +
      geom_line() + labs(title = paste("Évolution du Temps de(s) Patineur(se) par tour : \n",
                                       paste(input$player, collapse =", "),
                                       "\nPour les dates :\n",
                                       paste(input$date, collapse =", ")),
                         color = "Patineur(se)s") +
      scale_x_continuous(breaks = 1:16, labels = paste0("tour", 1:16)) +
      scale_y_continuous(breaks = seq(20, 70, by = 2)) +
      theme_minimal() +
      theme(legend.title = element_text("Patineur(se)s"), legend.position = "right")
    
    output$evolution_rang <- renderPlot({
      if ("RANGS" %in% input$Rang_time) {
        q
      } else {
        NULL
      }
    })
    
    output$evolution_temps <- renderPlot({
      if ("TIMES" %in% input$Rang_time) {
        p
      } else {
        NULL
      }
    })
    
    output$evolution_single <- renderPlot({
      if ("RANGS" %in% input$Rang_time & "TIMES" %in% input$Rang_time) {
        NULL
      } else {
        if (!is.null(input$Rang_time)) {
          if (input$Rang_time == "RANGS") {
            q
          } else if (input$Rang_time == "TIMES") {
            p
          }
        }
      }
    })
    
    output$rang_time_tours <- renderText({
      G <- c()
      R <- c()
      
      for (L in 1:nrow(selected_Rang)) {
        G <- c(G,paste("->",paste(unlist(selected_Rang[L,]), collapse = ","),"\n")) 
      }
      for (L in 1:nrow(selected_Time)) {
        R <- c(R,paste("->",paste(unlist(selected_Time[L,]), collapse = ","),"\n")) 
      }
      O <- c()
      j <- c()
      for (p in input$player) {
        for (d in input$date) {
          j <- c(j,p)
          O <- c(O,paste("du date : ",d,":\n"))
        }
      }
      return(paste("Pour le Patineur(se/s)",j,
                   ", ",O,paste("Les temps :\n",R),
                   paste("Les Rang sont :\n",G),"\n",paste(input$Rang_time)))
    })
  })
}

shinyApp(ui = ui, server = server)
